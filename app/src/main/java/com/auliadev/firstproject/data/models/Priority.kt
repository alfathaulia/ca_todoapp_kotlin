package com.auliadev.firstproject.data.models

enum class Priority {
    HIGH,
    MEDIUM,
    LOW
}
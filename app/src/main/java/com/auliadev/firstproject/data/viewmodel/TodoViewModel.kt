package com.auliadev.firstproject.data.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.auliadev.firstproject.data.TodoDatabase
import com.auliadev.firstproject.data.models.TodoData
import com.auliadev.firstproject.data.repository.TodoRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class TodoViewModel(application: Application) : AndroidViewModel(application){
    private val todoDao = TodoDatabase.getDatabase(application).toDoDao()
    private val repository: TodoRepository = TodoRepository(todoDao)
    val getAllData: LiveData<List<TodoData>> = repository.getAllData
    val sortByHighPriority: LiveData<List<TodoData>> = repository.sortByHighPriority
    val sortByLowPriority: LiveData<List<TodoData>> = repository.sortByLowPriority

    fun insertData(todoData: TodoData) {
        viewModelScope.launch (Dispatchers.IO ){
            repository.insertData(todoData)
        }
    }

    fun updateData(todoData: TodoData) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.updateData(todoData)
        }
    }

    fun deleteData(todoData: TodoData) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteItem(todoData)
        }
    }

    fun deleteAll() {
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteAll()
        }
    }

    fun searchData(queryString: String) : LiveData<List<TodoData>>{

            return repository.searchData(queryString)

    }
}
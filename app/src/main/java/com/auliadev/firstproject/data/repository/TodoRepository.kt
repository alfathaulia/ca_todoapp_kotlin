package com.auliadev.firstproject.data.repository

import androidx.lifecycle.LiveData
import com.auliadev.firstproject.data.TodoDao
import com.auliadev.firstproject.data.models.TodoData

class TodoRepository(private  val todoDao: TodoDao) {
    val getAllData: LiveData<List<TodoData>> = todoDao.getAllData()
    val sortByHighPriority: LiveData<List<TodoData>> = todoDao.sortByHighPriority()
    val sortByLowPriority: LiveData<List<TodoData>> = todoDao.sortByLowPriority()
    suspend fun insertData(todoData : TodoData) {
        todoDao.insertData(todoData)
    }
    suspend fun updateData(todoData: TodoData) {
        todoDao.updateData(todoData)
    }

    suspend fun deleteItem(todoData: TodoData) {
        todoDao.deleteData(todoData)
    }

    suspend fun  deleteAll() {
        todoDao.deleteAll()
    }
     fun searchData(queryString: String) : LiveData<List<TodoData>>{
        return todoDao.searcData(queryString)
    }

}